<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Post extends General_Controller {

    public function index() {
        $event = $this->input->post("is_event");        
        if ($event) {
            $insert['message'] = json_encode(array('target_el' => $this->input->post("target_el"), 'target_container' => $this->input->post("target_container")));
        } else {
            $insert['message'] = $this->input->post("message");
        }
        $insert['is_event'] = (int) $this->input->post("is_event");
        $insert['target'] = $this->input->post("target");
        $insert['to_room_id'] = $this->input->post("to_room_id");
        $insert['from_user_id'] = $this->user_data->id;
        $this->db->insert("messages", $insert);
        echo $this->db->insert_id();
    }

}

/* End of file post.php */
/* Location: ./application/controllers/post.php */