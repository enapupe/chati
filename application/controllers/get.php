<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Get extends General_Controller {

    public function index() {
        $return = null;
        $to_room_id = $this->input->post("to_room_id");
        $new_messages = $this->db->query("SELECT a.*, c.name AS user_name, b.user_id FROM messages a LEFT JOIN message_users b ON b.user_id = ".$this->user_data->id." AND b.message_id = a.id LEFT JOIN users c ON a.from_user_id = c.ID WHERE a.to_room_id = $to_room_id AND a.from_user_id != ".$this->user_data->id." AND b.user_id IS NULL");
        if ($new_messages->num_rows()){
            foreach($new_messages->result() as $message){
                $this->db->insert("message_users", array('user_id' => $this->user_data->id, 'message_id' => $message->id));
                if ($message->is_event == 1){
                    $event = json_decode($message->message);
                    $return[] = array('is_event' => true, 'target_el' => $event->target_el,  'target_container' => $event->target_container);
                } else {
                    $return[] = array('id' => $message->id, 'from_user' => $message->user_name, 'to_window'=> $message->target, 'is_event' => false, 'message' => $message->message);            
                }
            }
            echo json_encode($return);
        }
    }

}

/* End of file get.php */
/* Location: ./application/controllers/get.php */