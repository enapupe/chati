<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller {
    
}

/**
 * 
 * General controller
 * 
 */ 
class General_Controller extends MY_Controller {
    var $user_data = false;
    function __construct()
    {
        parent::__construct();
        $this->load->library("session");
        $this->load->model("user_model");
        $this->user_data = $this->user_model->get_user_data();
        if (!$this->user_data) {
            $this->session->set_userdata(array('current_url' => current_url()));
            redirect('setup');
        }
    }
}

/* End of file my_controller.php */
/* Location: ./application/controllers/my_controller.php */