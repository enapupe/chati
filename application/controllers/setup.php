<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Setup extends CI_Controller {

    public function index() {
        $this->load->view("setup_index");
    }
    function dosetup(){
        $this->load->library("session");
        $this->load->model("user_model");
        $name = $this->input->post("name");
        $this->user_model->set_user_data($name);
        redirect($this->session->userdata('current_url'));
    }
}

/* End of file setup.php */
/* Location: ./application/controllers/setup.php */