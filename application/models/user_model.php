<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends CI_Model {

    function get_user_data()
    {
        if ($this->session->userdata('loggedin'))
		{
		    $user_data['name'] = $this->session->userdata('name');
			$user_data['id'] = $this->session->userdata('id');
			$user_data['sessid'] = $this->session->userdata('session_id');
			return (object) $user_data;
        }
    }

    function set_user_data($name) {
        $this->db->insert("users", array('name' => $name));
        $user_id = $this->db->insert_id();
       
        $user_data['loggedin'] = true;
        $user_data['name'] = $name;
        $user_data['id'] = $user_id;
        $this->session->set_userdata($user_data);
        return true;
    }	
    function add_user_to_room($room){
        $this->db->where("user_id", $this->user_data->id);
        $this->db->where("room_id", $room);
        if ($this->db->get("room_user")->num_rows() == 0){
            $this->db->insert("room_user", array('room_id' => $room, 'user_id' => $this->user_data->id));
            $this->db->insert("messages", array('message' => $this->user_data->name . " joined room.", "from_user_id" => "-1", "to_room_id" => $room, "target" => "#console"));
        }
    }
}


/* End of file user_model.php */
/* Location: application/models/user_model.php */