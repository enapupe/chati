<!DOCTYPE HTML>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <title>Chatt</title>
    <link rel="stylesheet" href="<?php echo base_url();?>css/style.css" />
</head>
<body>
    <div id="container">
        <div id="chatwindow">
            <div id="drop_subject"></div>
            <div id="chatarea">
                <div id="console"></div>
                <div class="clearfix"></div>
                <div class="chatsubject focused" id="window0"></div>
            </div><!--end #chatarea-->
            <div id="chatinput">
                <textarea name="input" id="input" cols="30" rows="10"></textarea>
                <img src="http://www4.uwm.edu/commonspot/dashboard/images/dialog/loading.gif" id="loading" style="display:none" />
            </div><!--end #chatinput-->
        </div><!--end #chatwindow-->
    </div><!--end #container-->
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.8.0.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>js/jquery-ui-1.8.23.custom.min.js"></script>
    <script type="text/javascript">
        
        var chat_input = $("#chatinput textarea");
        chat_input.on('keydown', function(e){
            var a = $(this);
            if (e.which === 13){
                e.preventDefault();
                var msg = a.val();
                if (msg.length){
                    e.preventDefault();
                    $("#chatarea .focused").appendChat(msg, false);
                    a.val('');
                }
                return false;
            }
        });
        
        
        $("#chatarea").on('click', '.chatsubject', function(){
            var a = $(this);
            $("#chatarea .chatsubject").removeClass("focused");
            a.addClass("focused");
            chat_input.focus();
        });
        
//TODO: IMPLEMENT SIMPLIFIED HASH SYSTEM
//TODO: <user> entered/left chat
        var from_id = <?php echo $this->user_data->id;?>;
        var to_room_id = <?php echo $room;?>;
        var post_url = "<?php echo base_url();?>post";
        var get_url = "<?php echo base_url();?>get";
        
        
//        window.onbeforeunload = function() { 
//            return "";
//         }

        
        var chat_listener = function (){
            clearInterval(checknews);
            $.post(get_url, {to_room_id: to_room_id}, function(callback){
                if (callback)
                    $.each(callback, function(i,e){
                        if (e.is_event){
                            var target_el = $(e.target_el);
                            if (e.target_container == "new_window"){
                                drop_function(false, {draggable: $(target_el).find("span")});   
                            } else {
                                $(e.target_container).append(target_el);
                            }
                        } else {
                            $(e.to_window).appendChat(e.message, e.from_user, e.id);
                        }
                    });
                checknews = setInterval(chat_listener, 900);
            }, 'json');
        }
        var checknews = setInterval(chat_listener, 1900);
        chat_listener();
        
        var document_title = $("title");
        var default_title = document_title.text();
        var animate_title = function(msg){
            if (focused)
                return;
            var toggle = false;
            var blink = setInterval(function(){
                if (focused){
                    clearInterval(blink);
                    toggle = true;
                }
                if (!toggle) {
                    document_title.text(msg);
                    toggle = true;
                } else {
                    document_title.text(default_title);
                    toggle = false;
                }
            }, 1000);
        }
        
        var focused = true;
        $(window).focus(function(){
            focused = true;
        });
        $(window).blur(function(){
            focused = false;
        });
        
        var i = 0;
        $.fn.appendChat = function(msg, received, e){
            var current_subject = $(this);
            var callback = function(e){
                var append = $("<p>").hide();
                var le_drag = $("<span>").addClass("ledrag").appendTo(append);
                append.attr('id', 'cid-'+e);
//TODO SKIP <FROM> IF IT'S IN THE SAME 'BUFFER''
                append.append('<strong>'+received+':</strong> '+msg);
//TODO: SORT BASED ON TSTAMP
                append.appendTo(current_subject).fadeIn();
                le_drag.draggable({
                    revert:true,
                    start: function(e, ui){
                        $("#drop_subject").show();
                    },
                    stop: function(){
                        $("#drop_subject").hide();                    
                    }
                });
                current_subject.animate({scrollTop: 99999}, 200);
                current_subject = le_drag.draggable( "option", "containment" );
                i++;
            }
            if (!received) {
                received = "me";
//TODO: [ENTER] TIMESTAMP - REAL TIMESTAMP PREVENTING MIXES FROM LAG
//TODO: LOADING SPINNER WHILE POSTING
                $("#loading").show();
                $.post(post_url, {target: "#"+$(this).attr("id"), to_room_id: to_room_id, message: msg}, function(e){
                    callback(e);
                    $("#loading").hide();
                });
                
            } else {
                animate_title(received+": "+msg);
                callback(e);
            }
            return this;
        }
        var rebalance_chats = function(){
//TODO: HIDE/REMOVE COLUMNS - REMOVE MUST BE CONFIRMED BY BOTH USERS
            var subjects = $("#chatarea .chatsubject");
            subjects.each(function(i){
                var a = $(this);
                if (subjects.length && (i+1) < subjects.length) {
                    a.addClass("bordered");
                }
                if (!a.find("p").length){
                    a.remove();
                    subjects = $("#chatarea .chatsubject");
                }
            })
            subjects.removeClass("focused");
            var total_subjects = subjects.length;
            var width = Math.floor(100 / total_subjects);
            subjects.css("width", width + "%");
            subjects.animate({scrollTop: 99999}, 200);
        }
        
        $("#chatarea .chatsubject").droppable({
            drop: function(event, ui){
                //PREVENT DROP ON THE SAME CONTAINER
                var le_p = ui.draggable.parent();
                if (le_p.parent().attr("id") == $(this).attr("id"))
                    return;
                $(this).append(le_p);
                rebalance_chats();
                $(this).addClass("focused");
                $.post(post_url, {to_room_id: to_room_id, is_event: 1, target_el: "#"+le_p.attr("id"), target_container: "#"+$(this).attr("id")});
            }
        });

        
        var window_counter = 0;
        $("#drop_subject").droppable({
            drop: function( event, ui ) {
                window_counter ++;
                $(this).trigger('dropthis',[event, ui]);
                $(this).hide();
                var new_subject = $("<div>");
                new_subject.attr("id", "window"+window_counter );
                new_subject.addClass("chatsubject").appendTo( $("#chatarea") );
                var le_p = ui.draggable.parent();
                new_subject.append(le_p);
                new_subject.droppable({
                    drop: function(event, ui){
                        var le_p = ui.draggable.parent();
                        if (le_p.parent().attr("id") == $(this).attr("id"))
                            return;
                        $(this).append(le_p);
                        rebalance_chats();
                        $(this).addClass("focused");
                        $.post(post_url, {to_room_id: to_room_id, is_event: 1, target_el: "#"+le_p.attr("id"), target_container: "#"+$(this).attr("id")});
                    }
                });
                rebalance_chats();
                new_subject.addClass("focused");
                if (event)
                    $.post(post_url, {to_room_id: to_room_id, is_event: 1, target_el: "#"+le_p.attr("id"), target_container: "new_window"});
            }
        });
        var drop_function = $("#drop_subject").droppable('option', 'drop');
        
    </script>
</body>
</html>