<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Chat extends General_Controller {

    public function room($hash = false) {
        $this->db->where("hash", $hash);
        $data['room'] = $this->db->get("room")->row('id');
        $this->user_model->add_user_to_room($data['room']);
        $this->load->view("chat_room", $data);
    }
    public function index(){
        $hash = uniqid();
        $this->db->insert("room", array('hash' => $hash));
        redirect("chat/room/".$hash);        
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */